input-pad (1.0.99.20210817-3) UNRELEASED; urgency=medium

  * Set upstream metadata fields: Repository-Browse.
  * Update standards version to 4.6.1, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Fri, 11 Nov 2022 14:18:18 -0000

input-pad (1.0.99.20210817-2) unstable; urgency=medium

  * Team upload.
  * debian/patches: Add patch to make the build reproducible.
    (Closes: #998313)

 -- Boyuan Yang <byang@debian.org>  Tue, 02 Nov 2021 13:30:27 -0400

input-pad (1.0.99.20210817-1) unstable; urgency=medium

  * Team upload.
  * Upload to unstable. (Closes: #902814)

 -- Boyuan Yang <byang@debian.org>  Sat, 30 Oct 2021 20:01:36 -0400

input-pad (1.0.99.20210817-1~exp1) experimental; urgency=medium

  * Team upload.
  * New upstream version 1.0.99.20210817.
  * Rename packages following library name change:
    + libinput-pad1 -> libinput-pad-1.0-1
    + gir1.2-inputpad-1.0 -> gir1.2-inputpad-1.1
  * debian/libinput-pad-1.0-1.symbols: Document symbols.
  * debian/copyright: Refresh using machine-readable format.

 -- Boyuan Yang <byang@debian.org>  Wed, 27 Oct 2021 17:10:25 -0400

input-pad (1.0.3-5) unstable; urgency=medium

  * Team upload.

  [ Gunnar Hjalmarsson ]
  * Team upload
  * debian/rules: Build with gir (closes: #991087)

  [ Boyuan Yang ]
  * debian/patches: Drop empty dir.
  * debian/rules: Drop as-needed linker flag.
  * Bump Standards-Version to 4.6.0, dh v13.
  * debian/not-installed: Update file list.

 -- Boyuan Yang <byang@debian.org>  Wed, 27 Oct 2021 11:52:05 -0400

input-pad (1.0.3-4) unstable; urgency=medium

  * Team upload.
  * Upload to unstable
  * Remove python*-input-pad binary package (Closes: #936736)
    - Not worth to fix issues. There's no rdep.

 -- Changwoo Ryu <cwryu@debian.org>  Tue, 24 Dec 2019 07:47:35 +0900

input-pad (1.0.3-4~exp2) experimental; urgency=medium

  * Team upload.
  * debian/salsa-ci.yml: Add salsa CI config
  * gir1.2-inputpad-1.0: Renamed from gir1.2-input-pad-1.0, following the
    convention.
    - There was no rdep so transition dummy is not required
  * debian/control:
    - Bump Standards-Version to 4.4.1
    - Bump debhelper compat to 12.
    - Add "Rules-Requires-Root: no"
    - Fix spelling in Description
    - libinput-pad-dev Depends on gir1.2-inputpad-1.0
  * debian/compat: Dropped
  * debian/libinput-pad-xtest.symbols: Dropped. Not a shlib.
  * debian/rules:
    - Remove dh_makeshlib -Nlibinput-pad-xtest
    - Enable hardening build
  * debian/libinput-pad1.symbols: Add Build-Depends-Package field
  * debian/gir1.2-inputpad-1.0.install: Install typelib in the multi-arch
    directory

 -- Changwoo Ryu <cwryu@debian.org>  Wed, 11 Dec 2019 13:23:16 +0900

input-pad (1.0.3-4~exp1) experimental; urgency=medium

  * Team upload.
  * Use python 3 (Closes: #936736)
    - Drop python-input-pad and make python3-input-pad

 -- Changwoo Ryu <cwryu@debian.org>  Wed, 04 Dec 2019 08:52:18 +0900

input-pad (1.0.3-3) unstable; urgency=medium

  * Team upload.
  * debian/install: Install a debian-specific desktop file to start
    input-pad GUI application easily.

 -- Boyuan Yang <byang@debian.org>  Tue, 27 Nov 2018 16:37:51 -0500

input-pad (1.0.3-2) unstable; urgency=medium

  * Team upload.
  * debian:
    + Apply "wrap-and-sort -abst".
    + Bump debhelper compat to v11.
  * debian/control:
    + Remove LI Daobing from uploaders list. (Closes: #841804)
    + Update maintainer address and use Debian Input Method Team.
      (Closes: #899796)
    + Bump Standards-Version to 4.2.1.
    + Update homepage and use the project on GitHub.
    + Update Vcs-* fields and use git repo under Salsa Input
      Method Team.
  * debian/watch: Monitor GitHub upstream project.
  * debian/lintian-overrides: Add override for providing
    python-input-pad .pth file.

 -- Boyuan Yang <byang@debian.org>  Fri, 23 Nov 2018 11:21:35 -0500

input-pad (1.0.3-1) unstable; urgency=medium

  * New upstream release
  * Bump standards version to 3.9.5
  * Use autoreconf. Closes: #733710
  * Use hardening flags.

 -- Osamu Aoki <osamu@debian.org>  Sun, 26 Jan 2014 22:24:52 +0900

input-pad (1.0.2-2) unstable; urgency=low

  * libinput-pad-dev should depend on libgtk-3-dev. Closes: #724226
  * python-gi is not needed for input-pad.

 -- Osamu Aoki <osamu@debian.org>  Sun, 06 Oct 2013 02:27:18 +0900

input-pad (1.0.2-1) unstable; urgency=low

  * New upstream release
  * Bump standards version to 3.9.4 with compat 9 and python2.
  * Update symbols and build with GTK+3.
  * Do not ship *.a nor *.la.
  * Add the gir1.2-input-pad-1.0 package for GObject introspection.

 -- Osamu Aoki <osamu@debian.org>  Sun, 15 Sep 2013 16:17:55 +0900

input-pad (1.0.1-2) unstable; urgency=low

  * Fix "Spelling mistake in package description and package description
    form suggestion" (Closes: #623853)
  * Fix "package-contains-empty-directory usr/lib/input-pad-
    1.0/modules/kbdui/" (Closes: #631644)

 -- Asias He <asias.hejun@gmail.com>  Mon, 06 Feb 2012 03:59:39 +0000

input-pad (1.0.1-1) unstable; urgency=low

  * New upstream release
  * Bump standards version to 3.9.2

 -- Asias He <asias.hejun@gmail.com>  Wed, 06 Jul 2011 22:17:07 +0800

input-pad (1.0.0-2) unstable; urgency=low

  * Fix "Please don't install la file in dev-package (Policy 10.2)"
    (Closes: #620771)
  * Fix "Getting rid of unneeded *.la / emptying dependency_libs"
    (Closes: #621703)

 -- Asias He <asias.hejun@gmail.com>  Sun, 10 Apr 2011 09:56:59 +0800

input-pad (1.0.0-1) unstable; urgency=low

  * New upstream release
  * Set Vcs to git.debian.org
  * Bump libinput-pad0 to libinput-pad1
  * Set DM-Upload-Allowed to yes

 -- Asias He <asias.hejun@gmail.com>  Sun, 20 Mar 2011 09:46:10 +0800

input-pad (0.1.2-1) unstable; urgency=low

  [ Asias He ]
  * New upstream release.
  * debian/control:
    - updated Depends for libinput-pad0 and lininput-pad-xtest

 -- LI Daobing <lidaobing@debian.org>  Sat, 25 Sep 2010 20:46:56 +0800

input-pad (0.1.1-1) unstable; urgency=low

  * Initial release (Closes: #593949)

 -- Asias He <asias.hejun@gmail.com>  Mon, 23 Aug 2010 12:20:21 +0800
